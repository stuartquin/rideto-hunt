#!/bin/bash

cd ./client
NODE_ENV=production yarn build
cd ../

cp client/build/static/js/main.js ridetohunt/static/js
cp client/build/static/css/main.css ridetohunt/static/css
