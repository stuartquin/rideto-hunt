# RideTo Product App

https://vast-anchorage-51712.herokuapp.com/

## Features

* Product listing with user votes
* Popular, Newest and Featured ordering
* Django based rest API
* Admin interface
* Import from ProductHunt
* Responsive design

## Build and Deploy

The app is hosted on Heroku.

Loading data from ProductHunt API is optional and requires an API Token
https://api.producthunt.com/v1/docs

After making changes on master branch:

```
# Compile JS/CSS assets
./build.sh

# Commit them in - NOTE this should be changed as soon as possible
git commit -a

# Push to heroku origin, this will trigger a release
git push origin heroku

# Load data from PH
heroku run python ridetohunt/manage.py fetch_products <API_TOKEN>
```

## Developing

The app is divided into 2 parts - client and server.

*Client*

```
cd ./client

# optional - ensure correct node version etc.
nvm use

# Start development server
yarn start

# Run tests
yarn test
```

*Server*

Requirements are stored in `./ridetohunt/requirements.txt`

```
python ridetohunt/manage.py runserver
```

## Notes

* Client is ReactJS with CSS modules.
* Server is Python 3 + Django + DRF
* The client app does not currently support user authentication. When a vote
is cast the ID is stored in localStorage to prevent double voting.
* Client assets are currently compiled on local dev machine and committed
before deploying to Heroku, this should be improved to avoid committing in
compiled assets.
