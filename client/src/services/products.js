import { buildRequest } from './api';
import storage from './storage';

const fetchProducts = (page = 1, order = 'created_at') => {
  const req = buildRequest('product', 'GET', null, {page, order});

  return fetch(req).then(res => res.json());
};

const fetchProduct = (id) => {
  const req = buildRequest(`product/${id}`, 'GET');

  return fetch(req).then(res => res.json());
};

const hasVoted = (product) => {
  const voted = storage.load('voted') || [];

  return voted.indexOf(product.id) > -1;
};

const vote = (product) => {
  const req = buildRequest(`product/${product.id}`, 'PUT')

  return fetch(req).then((res) => {
    const voted = storage.load('voted') || [];
    storage.store('voted', voted.concat([product.id]));

    return res.json();
  });
};

export default {
  fetchProducts,
  fetchProduct,
  hasVoted,
  vote,
};
