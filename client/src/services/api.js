const API_URL = location.hostname === 'localhost' && location.port === '3000' ?
  'http://localhost:8000/api/' :
  '/api/';

const getCSRFToken = () => {
  const el = document.querySelector('input[name=csrfmiddlewaretoken]');
  return el ? el.value : null;
};

const CSRF_TOKEN = getCSRFToken();

const getRequestMeta = (method) => {
  return {
    method,
    headers: new Headers({
      'Content-Type': 'application/json',
      'X-CSRFToken': CSRF_TOKEN,
    })
  };
};

const parseParams = (params) => {
  const parsed = {};
  Object.keys(params).forEach((key) => {
    const param = params[key];

    if (param.toISOString) {
      parsed[key] = param.toISOString().substring(0, 10);
    } else {
      parsed[key] = param;
    }
  });

  return parsed;
};

export const buildRequest = (
  path, method = 'GET', data = null, query = {}
) => {
  const meta = getRequestMeta(method);
  const parsed = parseParams(query);
  const search = Object.keys(parsed).reduce(
    (params, key) => `${params}${key}=${parsed[key]}&`, '?'
  ).slice(0, -1);

  const url = `${API_URL}${path}/${search}`;

  if (data) {
    meta.body = JSON.stringify(data);
  }

  return new Request(url, meta);
};
