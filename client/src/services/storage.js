const store = (key, value) => {
  if (!window.localStorage || !window.localStorage.setItem) {
    console.warn('Storage not available');
    return null;
  }

  return window.localStorage.setItem(key, JSON.stringify(value));
};

const load = (key) => {
  if (!window.localStorage || !window.localStorage.getItem) {
    console.warn('Storage not available');
    return null;
  }

  return JSON.parse(window.localStorage.getItem(key));
};

export default {
  store,
  load,
};
