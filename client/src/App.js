import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import styles from './App.module.css';

import ProductList from './components/ProductList';

class App extends Component {
  render() {
    return (
      <Router>
        <div className={styles.app}>
          <Route path="" component={ProductList} />
        </div>
      </Router>
    );
  }
}

export default App;
