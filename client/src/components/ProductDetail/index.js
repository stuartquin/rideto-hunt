import React from 'react';
import PropTypes from 'prop-types';

import productsService from 'services/products';
import Button from 'components/Button';
import TagList from 'components/TagList';
import styles from './ProductDetail.module.css';

const ProductDetail = ({ product, onVote }) => {
  const date = product.created_at.substring(0, 10);

  return (
    <div className={styles.productDetail}>
      <div className={styles.content}>
        <img src={product.screen_shot} />
      </div>
      <div className={styles.actions}>
        <a
          href={product.redirect_url}
          className={styles.visit}
          target="_blank"
        >
          Visit
        </a>
        <Button
          onClick={() => onVote(product)}
          disabled={productsService.hasVoted(product)}
        >
          <span>↑ Upvote {product.votes}</span>
        </Button>
      </div>
      <div className={styles.info}>
        <div className={styles.infoRow}>
          <strong>Created:</strong><span>{date}</span>
        </div>
        <div className={styles.infoRow}>
          <strong>User:</strong><span>{product.username}</span>
        </div>
      </div>
      <div className={styles.tags}>
        <h4>Topics</h4>
        <TagList tags={product.topics} />
      </div>
    </div>
  );
};

export default ProductDetail;
