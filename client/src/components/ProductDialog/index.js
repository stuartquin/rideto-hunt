import React from 'react';

import productsService from 'services/products';
import styles from './ProductDialog.module.css';
import ProductDetail from 'components/ProductDetail';
import Spinner from 'components/Spinner';

class ProductDialog extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      product: null,
      isLoading: true,
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleVote = this.handleVote.bind(this);
  }

  componentDidMount () {
    const { match } = this.props;

    productsService.fetchProduct(match.params.id).then((product) => {
      this.setState({ product, isLoading: false });
    });
  }

  handleClose () {
    this.props.history.goBack();
  }

  handleVote (product) {
    productsService.vote(product).then((updatedProduct) => {
      this.setState({ product: updatedProduct });
    });
  }

  render () {
    const { product, isLoading } = this.state;

    const content = isLoading ?
      <Spinner /> :
      <ProductDetail
        product={product}
        onVote={this.handleVote}
      />;

    const header = product ? (
      <div className={styles.title}>
        <img src={product.image} className={styles.headerImage} />
        <div className={styles.heading}>
          <h4>{product.name}</h4>
          <div className={styles.subHeading}>{product.description}</div>
        </div>
      </div>
    ) : null;

    return (
      <div className={styles.productDialog}>
        <div className={styles.overlay} onClick={this.handleClose} />
        <div className={styles.dialogContent}>
          <div className={styles.dialogHeader}>
            {header}
            <div className={styles.headerActions}>
              <span className={styles.actionIcon} onClick={this.handleClose}>
                ✕
              </span>
            </div>
          </div>
          <div className={styles.dialogBody}>{content}</div>
        </div>
      </div>
    );
  }
}

export default ProductDialog;
