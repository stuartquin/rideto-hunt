import React from 'react';
import { Route } from 'react-router-dom';

import productsService from 'services/products';

import ProductDialog from 'components/ProductDialog';
import Spinner from 'components/Spinner';
import Button from 'components/Button';
import Product from './Product';
import Filters from './Filters';

import styles from './ProductList.module.css';

class ProductList extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isLoading: true,
      products: [],
      total: 0,
      order: '-created_at',
    }

    this.handleVote = this.handleVote.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount () {
    this.fetchProducts();
  }

  fetchProducts () {
    const { order } = this.state;
    const page = 1;

    productsService.fetchProducts(page, order).then(({ results, count }) => {
      this.setState({
        products: results,
        total: count,
        isLoading: false,
      });
    });
  }

  handleVote (product) {
    productsService.vote(product).then((updatedProduct) => {
      const products = [...this.state.products];
      const index = products.findIndex(p => p.id === updatedProduct.id);

      products[index] = {
        ...updatedProduct,
      };
      this.setState({ products });
    });
  }

  handleLoadMore () {
    const { products, order } = this.state;
    const page = Math.ceil(products.length / 10) + 1;

    productsService.fetchProducts(page, order).then(({ results, count }) => {
      this.setState({
        products: this.state.products.concat(results),
        total: count,
      });
    });
  }

  handleFilter (order) {
    this.setState({ order, isLoading: true }, () => this.fetchProducts());
  }

  render () {
    const { isLoading, products, total, order } = this.state;

    return (
      <div className={styles.productList}>
        <div className={styles.heading}>
          <h3>Products</h3>
          <Filters
            selected={order}
            onSelect={this.handleFilter}
          />
        </div>

        {isLoading ? (
          <Spinner />
        ): (
          <React.Fragment>
            <div className={styles.list}>
              {products.map(
                product => (
                  <Product
                    key={product.id}
                    product={product}
                    onVote={this.handleVote}
                  />
              ))}
            </div>

            {total > products.length ? (
              <Button onClick={this.handleLoadMore}>
                Load more Products
              </Button>
            ): null}
          </React.Fragment>
        )}

        <Route
          path="/product/:id"
          component={ProductDialog}
          onVote={this.handleVote}
        />
      </div>
    );
  }
}

export default ProductList;
