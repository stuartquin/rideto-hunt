import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Votes from './Votes';

Enzyme.configure({ adapter: new Adapter() });

it('Render Votes', () => {
  const wrapper = shallow(
    <Votes votes={1} onClick={jest.fn()} />
  );

  expect(wrapper.text()).toBe('^ 1');
});
