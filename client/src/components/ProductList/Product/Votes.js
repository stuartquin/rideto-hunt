import React from 'react';
import PropTypes from 'prop-types';

import styles from './Votes.module.css';

const Votes = ({ votes, disabled, onClick }) => {
  return (
    <button
      title="Vote"
      className={styles.votes}
      onClick={onClick}
      disabled={disabled}
    >
      ↑ {votes}
    </button>
  );
};

Votes.propTypes = {
  votes: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
}

export default Votes;
