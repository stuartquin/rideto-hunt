import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import productsService from 'services/products';

import Votes from './Votes';
import styles from './Product.module.css';

const Product = ({ product, onVote }) => {
  const featured = product.featured ? '🟐' : '';

  return (
    <div className={styles.product}>
      <div className={styles.image}>
        <img src={product.image} />
      </div>
      <Link to={`/product/${product.id}`} className={styles.details}>
        <strong>{product.name} {featured}</strong>
        <div>
          {product.description}
        </div>
      </Link>
      <Votes
        votes={product.votes}
        disabled={productsService.hasVoted(product)}
        onClick={() => onVote(product)}
      />
    </div>
  );
};

Product.propTypes = {
  product: PropTypes.object.isRequired,
  onVote: PropTypes.func.isRequired,
}

export default Product;
