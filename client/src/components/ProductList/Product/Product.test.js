import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { MemoryRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import Product from './index';

Enzyme.configure({ adapter: new Adapter() });

const product = {
  id: 1,
  name: 'Test Product',
  description: 'This is at test',
  votes: 6,
};

it('Render product with Link', () => {
  const onVote = jest.fn();
  const wrapper = mount(
    <MemoryRouter>
      <Product product={product} onVote={onVote} />
    </MemoryRouter>
  );

  const link = wrapper.find('a').first();
  expect(link.props().href).toMatch(/\/product\/1$/);
});
