import React from 'react';
import PropTypes from 'prop-types';

import styles from './Filters.module.css';

const FILTERS = {
  votes: 'Popular',
  created_at: 'Newest',
  featured: 'Featured',
};

const handleSelect = (filter, selected, onSelect) => {
  if (selected.replace('-', '') === filter && selected.startsWith('-')) {
    onSelect(filter);
  } else {
    onSelect(`-${filter}`);
  }
};

const getIconLabel = (selected, label) => {
  return selected.startsWith('-') ? `▾ ${label}` : `▴ ${label}`;
};

const Filters = ({ selected, onSelect }) => {
  const filters = Object.keys(FILTERS).map((filter) => {
    const isActive = selected.replace('-', '') === filter;
    const className = isActive ?
      `${styles.filter} ${styles.active}` :
      styles.filter;
    const label = isActive ?
      getIconLabel(selected, FILTERS[filter]) :
      FILTERS[filter];

    return (
      <a
        key={filter}
        className={className}
        onClick={() => handleSelect(filter, selected, onSelect)}
      >
        {label}
      </a>
    );
  });

  return (
    <div className={styles.filters}>
      {filters}
    </div>
  );
};

Filters.propTypes = {
  selected: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default Filters;
