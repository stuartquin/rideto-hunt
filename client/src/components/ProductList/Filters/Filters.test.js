import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Filters from './index';

Enzyme.configure({ adapter: new Adapter() });

it('Renders Filters', () => {
  const wrapper = shallow(
    <Filters selected="created_at" onSelect={jest.fn()} />
  );

  expect(wrapper.find('a').length).toBe(3);
  expect(wrapper.find('.active').text()).toBe('Newest');
});

describe('onClick', () => {
  test('Sets selected', () => {
    const onSelect = jest.fn();
    const wrapper = shallow(
      <Filters selected="created_at" onSelect={onSelect} />
    );

    wrapper.find('a').first().simulate('click');
    expect(onSelect).toHaveBeenCalledWith('-votes');
  });

  test('Inverts currently selected', () => {
    const onSelect = jest.fn();
    const wrapper = shallow(
      <Filters selected="-votes" onSelect={onSelect} />
    );

    wrapper.find('a').first().simulate('click');
    expect(onSelect).toHaveBeenCalledWith('votes');
  });
});
