import React from 'react';

import styles from './TagList.module.css';

const TagList = ({ tags }) => {
  return (
    <div className={styles.tagList}>
      {tags.map(tag => (
        <span key={tag} className={styles.tag}>{tag}</span>
      ))}
    </div>
  );
};

export default TagList;
