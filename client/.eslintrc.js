module.exports = {
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "jest": true,
    "node": true,
  },
  "plugins": [
    "import",
    "react",
  ],
  "settings": {
    "import/resolver": {
      "node": {
        "paths": ["./src"]
      }
    }
  },
  "rules": {
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "jsx-a11y": "off",
    "jsx-a11y/href-no-hash": "off",
    "jsx-a11y/label-has-for": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "react/forbid-prop-types": "off",
    "react/jsx-filename-extension": "off",
    "react/jsx-wrap-multilines": "off",
    "react/require-default-props": "off",
    "space-before-function-paren": "off",
    "function-paren-newline": "off",
    "comma-dangle": "off",
    "quotes": "off",
    "no-confusing-arrow": "off",
    "arrow-body-style": "off",
  }
};
