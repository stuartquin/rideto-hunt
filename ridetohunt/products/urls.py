from django.conf.urls import url
from products import views

urlpatterns = [
    url(r'(?P<pk>[0-9]+)/$', views.ProductDetailView.as_view()),
    url(r'^$', views.ProductView.as_view()),
]
