from django.contrib import admin

from products import models

@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'created_at')


@admin.register(models.Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('name', )
