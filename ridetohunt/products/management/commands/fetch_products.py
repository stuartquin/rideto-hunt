import json
import urllib.parse
import urllib.request

from django.core.management.base import BaseCommand, CommandError
from products import models

class Command(BaseCommand):
    help = 'Load products from PH API'

    def add_arguments(self, parser):
        parser.add_argument('token', nargs=1, type=str)


    def set_topics(self, product, topics):
        for topic in topics:
            db_topic = models.Topic.objects.filter(name=topic['name']).first()

            if not db_topic:
                db_topic = models.Topic()

            db_topic.name = topic['name']
            db_topic.save()

            product.topics.add(db_topic)


    def save_product(self, post):
        product = models.Product.objects.filter(remote_id=post['id']).first()

        if not product:
            product = models.Product()

        product.name = post['name']
        product.image = post.get('thumbnail', {}).get('image_url')
        screen_shots = list(post.get('screenshot_url', {}).values())
        try:
            product.screen_shot = screen_shots[-1]
        except IndexError:
            product.screen_shot = None

        product.description = post['tagline']
        product.votes = post['votes_count']
        product.created_at = post['created_at']
        product.featured = post['featured']
        product.username = post.get('user', {}).get('username')
        product.remote_id = post['id']
        product.redirect_url = post['redirect_url']

        product.save()

        self.set_topics(product, post.get('topics', []))

        product.save()


    def save_products(self, posts):
        for post in posts:
            self.save_product(post)


    def fetch_products(self, token):
        url = 'https://api.producthunt.com/v1/posts/all'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % token,
            'Host': 'api.producthunt.com',
        }

        req = urllib.request.Request(url, None, headers)
        with urllib.request.urlopen(req) as response:
           data = response.read()
           products = json.loads(data.decode('utf8'))
           self.save_products(products['posts'])


    def handle(self, *args, **options):
        self.fetch_products(options['token'][0])
