from django.db import models

class Topic(models.Model):
    name = models.CharField(db_index=True, max_length=512)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=512)
    image = models.CharField(max_length=512, blank=True, null=True)
    screen_shot = models.CharField(max_length=512, blank=True, null=True)
    description = models.CharField(max_length=1024, blank=True, null=True)
    votes = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    featured = models.BooleanField(default=False)
    username = models.CharField(max_length=512)
    topics = models.ManyToManyField(Topic)
    remote_id = models.IntegerField(db_index=True, blank=True, null=True)
    redirect_url = models.CharField(max_length=512, blank=True, null=True)

    def __str__(self):
        return self.name
