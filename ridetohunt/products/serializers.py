from rest_framework import serializers

from products import models


class ProductSerializer(serializers.ModelSerializer):
    topics = serializers.StringRelatedField(many=True)

    class Meta:
        model = models.Product
        fields = '__all__'
