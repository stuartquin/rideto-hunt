import datetime

from django.shortcuts import get_object_or_404, render
from rest_framework import authentication, permissions, status, generics
from rest_framework.views import APIView
from rest_framework.response import Response

from products import serializers, models


def index(request):
    return render(request, 'index.html')


class ProductView(generics.ListCreateAPIView):
    serializer_class = serializers.ProductSerializer

    def get_queryset(self):
        params = self.request.query_params
        order = params.get('order', '-created_at')
        return models.Product.objects.order_by(order).all()


class ProductDetailView(APIView):
    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer

    def get_object(self):
        return get_object_or_404(models.Product, pk=self.kwargs["pk"])

    def put(self, request, pk, format=None):
        product = self.get_object()
        serializer = serializers.ProductSerializer(
            product, data={ 'votes': product.votes + 1 }, partial=True
        )

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def get(self, request, pk, format=None):
        serializer = serializers.ProductSerializer(self.get_object())
        return Response(serializer.data)
